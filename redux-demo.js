const redux = require('redux'); // importing from node.js

// reducer function it will be called by react libry  inputs:oldstate+dispatchedAction
// output : newState reducer function is the pure function 
//1. no side effects inside the reducer not http request or local storage
const counterReducer = (state = {counter:0 },action) => {
    if(action.type === 'increment')
    {
        return {
            counter: state.counter + 1,
        };
    }
    if(action.type === 'decrement')
    {
        return {
            counter : state.counter - 1,
        };
    }
    return state;
};


// creating store 
const store =redux.createStore(counterReducer); // which reducer is responsible for changing.

//console.log(store.getState());  // before dispatching the action..

const counterSubsciber = () => {
    const latestState = store.getState();  // it willl get the latest updated snapshots.
    console.log(latestState);
}
// redux must be aware of the subscription method by using subscribe method.

store.subscribe(counterSubsciber); // pointing to the method bcz it will executed by the redux


// dispatching the action for increment
store.dispatch( {
    type: 'increment'
});

// dispatching the action for decerement

store.dispatch({
    type:'decrement'
});